<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    protected $table = 'paises';
    public function hotel(){
    	return $this->hasMany('\App\Models\Hotel', 'pais', 'id');
    }
}
