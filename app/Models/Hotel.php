<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Hotel extends Model
{
    protected $table = 'hotel';
    public function pais(){
    	return $this->hasOne('\App\Models\Paises','id','pais');
    }
}
