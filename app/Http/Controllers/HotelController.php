<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Paises;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotel = Hotel::with('pais')->get();
        return  response()->json($hotel);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Instanciamos la clase Pokemons
        $hotel = new Hotel;
        //Declaramos el nombre con el nombre enviado en el request
        $hotel->nombre = $request->nombre;
        $hotel->descripcion = $request->descripcion;
        $hotel->estrellas = $request->estrellas;
        $hotel->pais = $request->pais;
        $hotel->ciudad = $request->ciudad;
        //Guardamos el cambio en nuestro modelo
        $hotel->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        $hotel = Hotel::where('pais',$hotel->pais)->where('ciudad',$hotel->ciudad)->get();
        return response()->json($hotel);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {

        $hotel = Hotel::where('pais',$request->params['pais'])->where('ciudad',$request->params['ciudad'])->get();
        return response()->json($hotel);
    }

    public function listFilters(Request $request){

        if($request->params['filter'] == 'price'){
            if($request->params['value'] == 1){
                $hotel = Hotel::where('precio', '<', 100)->get();
            } else {
                $hotel = Hotel::where('precio', '>', 100)->get();
            }
        }
        if($request->params['filter'] == 'star'){
            $hotel = Hotel::where('estrellas', $request->params['value'])->get();
        }
        return response()->json($hotel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        //
    }
}
