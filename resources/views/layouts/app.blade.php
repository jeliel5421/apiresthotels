<!DOCTYPE html>

<html lang="en" >
<!-- begin::Head -->
	<head>
	    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title>
	        Prueba APIREST Hoteles
	    </title>
	    <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('js/moment.min.js')}}"></script>
        <script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/popper.min.js"></script>
        <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/style.css')}}">
		<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
		
		<script>
			var baseUrl='{{url('/')}}';
		</script>
		<link rel="stylesheet" href="">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
	</head>
	<body>
		@include('layouts.partials.header')
		<div id="app">
			<apihotels></apihotels>
		 </div>
		 <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
		 
	</body>
</html>
