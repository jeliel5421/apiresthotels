$(document).ready(function(){

	$.ajax({
        url: baseUrl+'/api/paises',
        method: 'get',
        beforeSend: function() {
    	},
        success: function(result){
            $("#pais").empty();
            $.each(result,function(index,item){
                $('#pais').append(`<option value="${item.id}">${item.name}</option>`);
            })
        },
        error: function(result){
            console.log('error');
        }
    });
    $('#pais').on('change',function(e){
    	var pais = $('option:selected', '#pais').val();
    	$.ajax({
			url: baseUrl+'/api/ciudades/'+pais,
	        method: 'get',
	        beforeSend: function() {
	    	},
	        success: function(result){
	            $("#ciudad").empty();
	            $.each(result,function(index,item){
	                $('#ciudad').append(`<option value="${item.id}">${item.ciudad}</option>`);
	            })
	        },
	        error: function(result){
	            console.log('error');
	        }
    	});
    });
    $.ajax({
        url: baseUrl+'/api/typerooms',
        method: 'get',
        beforeSend: function() {
        },
        success: function(result){
            $("#typeroom").empty();
            $.each(result,function(index,item){
                $('#typeroom').append(`<option value="${item.id}">${item.nombre}</option>`);
            })
        },
        error: function(result){
            console.log('error');
        }
    });

    
})